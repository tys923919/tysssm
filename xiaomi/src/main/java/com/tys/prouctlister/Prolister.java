package com.tys.prouctlister;

import com.tys.pojo.ProductType;
import com.tys.service.Producttypeservicei;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.List;

/**
 * @author shkstart
 * @create 2021-11-14 13:46
 */
@WebListener
public class Prolister implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("applicationContext-*.xml");
        Producttypeservicei producttypeservicei= (Producttypeservicei) applicationContext.getBean("Producttypeservice");
        List<ProductType> productTypes=producttypeservicei.getall();
        servletContextEvent .getServletContext().setAttribute("ptlist", productTypes);
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
