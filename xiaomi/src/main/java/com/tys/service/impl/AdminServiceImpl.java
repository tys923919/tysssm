package com.tys.service.impl;

import com.tys.mapper.AdminMapper;
import com.tys.pojo.Admin;
import com.tys.pojo.AdminExample;
import com.tys.service.AdminService;
import com.tys.util.MD5Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author shkstart
 * @create 2021-11-12 0:55
 */
@Service
public class AdminServiceImpl implements AdminService {
    @Autowired
    private AdminMapper adminMapper;
    @Override
    public Admin login(String name, String pwd) {
        AdminExample adminExample = new AdminExample();
        adminExample.createCriteria().andANameEqualTo(name);
        List<Admin> admins = adminMapper.selectByExample(adminExample);
        if (admins.size()>0){
            Admin admin = admins.get(0);
            String md5 = MD5Util.getMD5(pwd);
            if (md5.equals(admin.getaPass())){
                return admin;
            }
        }
        return null;
    }

    @Override
    public int regist(Admin admin) {
        String md5 = MD5Util.getMD5(admin.getaPass());
        admin.setaPass(md5);
        int insert = adminMapper.insert(admin);
        return insert;
    }
}
