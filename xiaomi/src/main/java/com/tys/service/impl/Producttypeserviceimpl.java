package com.tys.service.impl;

import com.tys.mapper.ProductTypeMapper;
import com.tys.pojo.ProductType;
import com.tys.pojo.ProductTypeExample;
import com.tys.service.Producttypeservicei;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author shkstart
 * @create 2021-11-14 14:02
 */
@Service("Producttypeservice")
public class Producttypeserviceimpl  implements Producttypeservicei {
    @Resource
    private ProductTypeMapper  productTypeMapper;
    public List<ProductType> getall(){
        List<ProductType> productTypes = productTypeMapper.selectByExample(new ProductTypeExample());
        return productTypes;
    }
}
