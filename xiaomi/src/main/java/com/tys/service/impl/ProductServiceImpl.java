package com.tys.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.tys.mapper.AdminMapper;
import com.tys.mapper.ProductInfoMapper;
import com.tys.pojo.ProductInfo;
import com.tys.pojo.ProductInfoExample;
import com.tys.pojo.vo.Productinfovo;
import com.tys.service.ProductService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author shkstart
 * @create 2021-11-14 12:34
 */
@Service
public class ProductServiceImpl implements ProductService {
    @Resource
    private ProductInfoMapper productInfoMapper;
    @Override
    public PageInfo split(int i, int pageSize) {
       PageHelper.startPage(i,pageSize);
        ProductInfoExample productInfoExample = new ProductInfoExample();
        productInfoExample.setOrderByClause("p_id desc");
        List<ProductInfo> productInfos = productInfoMapper.selectByExample(productInfoExample);
        PageInfo<ProductInfo> productInfoPageInfo = new PageInfo<ProductInfo>(productInfos);
        return productInfoPageInfo;
    }

    @Override
    public int save(ProductInfo productInfo) {
        int insert = productInfoMapper.insert(productInfo);
        return insert;
    }

    @Override
    public ProductInfo one(int pid) {
        ProductInfo productInfo = productInfoMapper.selectByPrimaryKey(pid);
        return productInfo;
    }

    @Override
    public int update(ProductInfo info) {
        int insert = productInfoMapper.updateByPrimaryKeySelective(info);
        return insert;
    }

    @Override
    public int delete(int pid) {
        int i = productInfoMapper.deleteByPrimaryKey(pid);
        return i;
    }

    @Override
    public int deletebatch(String[] st) {
       int i= productInfoMapper.deletebatch(st);
        return i;
    }

    @Override
    public PageInfo ajaxsplit(Productinfovo productinfovo, int pageSize) {
        PageHelper.startPage(productinfovo.getPage(),pageSize);
        List<ProductInfo> list=productInfoMapper.ajaxsplit(productinfovo);
        PageInfo<ProductInfo> infoPageInfo=new PageInfo<ProductInfo>(list);
        return infoPageInfo;
    }
}
