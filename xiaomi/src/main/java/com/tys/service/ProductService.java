package com.tys.service;

import com.github.pagehelper.PageInfo;
import com.tys.pojo.ProductInfo;
import com.tys.pojo.vo.Productinfovo;

/**
 * @author shkstart
 * @create 2021-11-14 12:29
 */
public interface ProductService {
    PageInfo split(int i, int pageSize);

    int save(ProductInfo productInfo);

    ProductInfo one(int pid);

    int update(ProductInfo info);

    int delete(int pid);

    int deletebatch(String[] st);

    PageInfo ajaxsplit(Productinfovo productinfovo, int pageSize);
}
