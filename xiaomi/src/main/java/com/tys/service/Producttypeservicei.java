package com.tys.service;

import com.tys.pojo.ProductType;

import java.util.List;

/**
 * @author shkstart
 * @create 2021-11-14 14:06
 */
public interface Producttypeservicei {
    public List<ProductType> getall();
}
