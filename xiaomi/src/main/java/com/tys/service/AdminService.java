package com.tys.service;

import com.tys.pojo.Admin;

/**
 * @author shkstart
 * @create 2021-11-12 0:53
 */
public interface AdminService {
    Admin login(String name,String pwd);

    int regist(Admin admin);
}
