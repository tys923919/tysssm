package com.tys.controller;

import com.tys.pojo.Admin;
import com.tys.service.AdminService;
import com.tys.service.impl.AdminServiceImpl;
import com.tys.util.MD5Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * @author shkstart
 * @create 2021-11-12 1:10
 */
@Controller
@RequestMapping(value = "/admin")
public class AdmainController {
    @Autowired
    private AdminService adminService;
    @RequestMapping("/login.action")
    public String login(String name, String pwd ,HttpServletRequest req){
        System.out.println("改变了");
        System.out.println("改变了");
        System.out.println("改变了");
        System.out.println(name);
        System.out.println(pwd);
        Admin admin = adminService.login(name, pwd);
        System.out.println(admin.getaPass());
        if (admin!=null){//登录成功

            req.setAttribute("admin",admin);
            return "main";
        }else {//登录失败返回登录失败提示
            req.setAttribute("errmsg","用户名或密码错误");
            return "login";
        }

    }
    @RequestMapping(value = "/regist.action")
    public ModelAndView regist( Admin admin,int aName){
        System.out.println(aName);
        ModelAndView modelAndView = new ModelAndView();
        int num=-1;
        try {
            num= adminService.regist(admin);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (num>0){
            modelAndView.addObject("errmsg","注册成功");
            modelAndView.setViewName("main");
        }else if (admin.getaName().equals("")||admin.getaPass().equals("")){
            modelAndView.addObject("errmsg","用户名或密码不能为空");
            modelAndView.setViewName("regist");
        }
        return modelAndView;
    }
}
