package com.tys.controller;

import com.github.pagehelper.PageInfo;
import com.tys.pojo.ProductInfo;
import com.tys.pojo.vo.Productinfovo;
import com.tys.service.ProductService;
import com.tys.util.FileNameUtil;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author shkstart
 * @create 2021-11-14 12:24
 */
@Controller
@RequestMapping("/prod")
public class ProductController {
    public String saveFileName;
    public static final int  PAGE_SIZE = 5;
    @Resource
    private ProductService productService;
    @RequestMapping(value = "/split.action" )
    public String split(HttpServletRequest httpServletRequest ){
        PageInfo pageInfo=null;
        Productinfovo vo = (Productinfovo) httpServletRequest.getSession().getAttribute("vo");
        if (vo!=null){
             pageInfo=productService.ajaxsplit(vo,PAGE_SIZE);
            httpServletRequest.getSession().removeAttribute("vo");
        }else {
            pageInfo=productService.split(1,PAGE_SIZE);

        }
        httpServletRequest.setAttribute("info",pageInfo);
        return "product";
    }
    @ResponseBody
    @RequestMapping(value = "/ajaxsplit.action")
    public void ajaxsplit(Productinfovo productinfovo, HttpSession ss){
        //System.out.println(page+"dk fjk ldfk ljg");
        PageInfo pageInfo=productService.ajaxsplit(productinfovo,PAGE_SIZE);
        //PageInfo pageInfo=productService.split(page,PAGE_SIZE);
        ss.setAttribute("info",pageInfo);

    }
    @ResponseBody
    @RequestMapping(value = "/ajaxImg.action")
    public Object ajaxImg(MultipartFile pimage , HttpServletRequest request){
        //文件名
        saveFileName= FileNameUtil.getUUIDFileName()+FileNameUtil.getFileType(pimage.getOriginalFilename());
        //文件路径
        String path = request.getServletContext().getRealPath("/image_big");
        //文件转存
        try {
            pimage.transferTo(new File(path + File.separator + saveFileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        //为了在客户端显示图片，要将存储的文件名回传下去，由于是自定义的上传插件，所以此处要手工处理JSON
        JSONObject object = new JSONObject();
        object.put("imgurl",saveFileName);
        //切记切记：JSON对象一定要toString()回到客户端
        return object.toString();
    }

    @RequestMapping(value = "/save.action",method = RequestMethod.POST)
    public String save(ProductInfo productInfo,HttpServletRequest request){
        productInfo.setpDate(new Date());
        productInfo.setpImage(saveFileName);
       int coun= productService.save(productInfo);
       if (coun>0){
           request.getSession().setAttribute("msg","添加成功 ");
       }else {
           request.getSession().setAttribute("msg","添加失败 ");
       }
        saveFileName="";
       return "redirect:/prod/split.action";
    }
    @RequestMapping(value = "/one.action")
    public String one(Productinfovo productinfovo,int pid,Model model ,HttpSession session){

        ProductInfo productInfo=productService.one(pid);
        System.out.println(productInfo.getpImage());
        model.addAttribute("prod",productInfo);
     session.setAttribute("vo",productinfovo);
        return "update";
    }
    @RequestMapping(value = "/update.action")
    public String update(ProductInfo info, HttpServletRequest request){
        System.out.println(info.getpImage());
        if (!saveFileName.equals("")){//如果不等于空那么就证明客户端修改了图片就需要修改图片名称
            info.setpImage(saveFileName);
        }
        int num=-1;
        try {
            num=productService.update(info);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (num>0){
            request.getSession().setAttribute("msg","修改成功");
            PageInfo pageInfo=null;
            Productinfovo vo = (Productinfovo) request.getSession().getAttribute("vo");
            System.out.println(vo);
            if (vo!=null){
                pageInfo=productService.ajaxsplit(vo,PAGE_SIZE);

            }else {
                pageInfo=productService.split(1,PAGE_SIZE);

            }
            request.setAttribute("info",pageInfo);
            return "product";

        }else {
            request.getSession().setAttribute("msg","修改失败");
            return "update";
        }
        //return "redirect:/prod/split.action";
    }
    @RequestMapping(value = "/delete.action")
    public String delete( int pid,HttpServletRequest request){
        int num=-1;
        try {
            num=productService.delete(pid);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (num>0){
            request.getSession().setAttribute("msg","删除成功");
        }else {
            request.getSession().setAttribute("msg","删除失败");
        }
        return "forward:/prod/split.action";
    }
    @RequestMapping(value = "/deletebatch.action")
    public String deletebatch(String  str, HttpSession session){
        String [] st=str.split(",");

        int num=-1;
        try {
           num= productService.deletebatch(st);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (num>0){
            session.setAttribute("msg","删除成功");
        }else {
            session.setAttribute("msg","删除失败");
        }
        return "forward:/prod/split.action";
    }
}
