package com.tys.pojo.vo;

/**
 * @author shkstart
 * @create 2021-11-16 12:26
 */
public class Productinfovo {
    private Integer id;
    private Integer page;
    private Integer typeid;
    private Integer lprice;
    private Integer hprice;
    private String pname;

    @Override
    public String toString() {
        return "Productinfovo{" +
                "id=" + id +
                ", page=" + page +
                ", typeid=" + typeid +
                ", lprice=" + lprice +
                ", hprice=" + hprice +
                ", pname='" + pname + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getTypeid() {
        return typeid;
    }

    public void setTypeid(Integer typeid) {
        this.typeid = typeid;
    }

    public Integer getLprice() {
        return lprice;
    }

    public void setLprice(Integer lprice) {
        this.lprice = lprice;
    }

    public Integer getHprice() {
        return hprice;
    }

    public void setHprice(Integer hprice) {
        this.hprice = hprice;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }
}
